import java.util.Scanner;

public class Application {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your string.");
        String input = scan.nextLine();
        Sequence[] sequences = parse(input);
        print(sequences, 10);
    }

    public static void print(Sequence[] seq, int num) {
        int j = 0;
        for (Sequence sequence : seq) {
            j++;
            System.out.println();
            System.out.println("Sequence #" + j);
            for (int i = 1; i < num + 1; i++) {
                System.out.println("Term " + i + " is " + sequence.getTerm(i));
            }
        }
    }

    public static Sequence[] parse(String inputString) {

        String[] stringArray = inputString.split(";");
        int arraySize = stringArray.length / 4;
        Sequence[] returnArray = new Sequence[arraySize];

        int arrayPos = 0;
        for (int i = 0; i < stringArray.length; i += 4) {
            if (stringArray[i].equals("Fib")) {
                fibonacciSequence seq = new fibonacciSequence(Integer.parseInt(stringArray[i + 1]),
                        Integer.parseInt((stringArray[i + 2])));
                returnArray[arrayPos] = seq;
            } else if (stringArray[i].equals("Skip")) {
                skipSequence seq = new skipSequence(Integer.parseInt(stringArray[i + 1]),
                        Integer.parseInt((stringArray[i + 2])), Integer.parseInt((stringArray[i + 3])));
                returnArray[arrayPos] = seq;
            } else {
                subtractonacciSequence seq = new subtractonacciSequence(Integer.parseInt(stringArray[i + 1]),
                        Integer.parseInt((stringArray[i + 2])));
                returnArray[arrayPos] = seq;
            }
            arrayPos += 1;
        }

        return returnArray;
    }
}
