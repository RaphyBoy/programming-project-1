
public class subtractonacciSequence extends Sequence {
    private int num1;
    private int num2;

    public subtractonacciSequence(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public int getTerm(int inputLength) {
        // Declare the array
        int[] skipArray = new int[inputLength];
        // Assign the first index
        skipArray[0] = this.num1;
        if (inputLength == 1)
            return skipArray[0];
        // Assign the second index
        skipArray[1] = this.num2;
        if (inputLength == 2)
            return skipArray[1];
        // loops through and places all the numbers
        for (int i = 2; i < inputLength; i++) {
            int placingNum = skipArray[i - 2] - skipArray[i - 1];
            skipArray[i] = placingNum;
        }
        return skipArray[inputLength - 1];
    }

}
