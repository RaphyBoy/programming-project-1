import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
 
public class TESTskiponacciSequence{  
    
    @Test  
    public void normalInput(){
        skipSequence sequence = new skipSequence(1, 2, 3);

        assertEquals(3,sequence.getTerm(4));
    }

    @Test
    public void negativeInput(){
        skipSequence sequence = new skipSequence(-1, -2, -3);

        assertEquals(-3,sequence.getTerm(4));
    }

    @Test
    public void negAndPosInput(){
        skipSequence sequence = new skipSequence(-1, 2, -3);

        assertEquals(1,sequence.getTerm(4));
    }

    @Test
    public void doubleDigitInputs(){
        skipSequence sequence = new skipSequence(10, 20, 30);

        assertEquals(30,sequence.getTerm(4));
    }

    @Test
    public void doubleDigitTerm(){
        skipSequence sequence = new skipSequence(1, 2, 3);
        
        assertEquals(19,sequence.getTerm(10));
    }

    @Test
    public void getTerm1or2(){
        skipSequence sequence = new skipSequence(1, 2, 3);

        assertEquals(1, sequence.getTerm(1));
    }
}
