public class skipSequence extends Sequence {
    int num1;
    int num2;
    int num3;

    public skipSequence(int num1, int num2, int num3) {
        this.num1 = num1;
        this.num2 = num2;
        this.num3 = num3;
    }

    public int getTerm(int inputLength) {
        // Declare the array
        int[] skipArray = new int[inputLength];
        // Assign the first index
        skipArray[0] = this.num1;
        if (inputLength == 1)
            return skipArray[0];
        // Assign the second index
        skipArray[1] = this.num2;
        if (inputLength == 2)
            return skipArray[1];
        // Assign the third index
        skipArray[2] = num3;
        if (inputLength == 3)
            return skipArray[2];
        // loops through and places all the numbers
        for (int i = 3; i < inputLength; i++) {
            int placingNum = skipArray[i - 3] + skipArray[i - 2];
            skipArray[i] = placingNum;
        }
        return skipArray[inputLength - 1];
    }

}
