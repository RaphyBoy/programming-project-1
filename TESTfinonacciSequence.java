import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TESTfinonacciSequence {

    @Test
    public void normalInput() {
        fibonacciSequence sequence = new fibonacciSequence(5, 6);

        assertEquals(11, sequence.getTerm(3));
    }

    @Test
    public void negativeInput() {
        fibonacciSequence sequence = new fibonacciSequence(-5, -6);

        assertEquals(-17, sequence.getTerm(4));
    }

    @Test
    public void negAndPosInput() {
        fibonacciSequence sequence = new fibonacciSequence(-5, 6);

        assertEquals(7, sequence.getTerm(4));
    }

    @Test
    public void doubleDigitInputs() {
        fibonacciSequence sequence = new fibonacciSequence(50, 60);

        assertEquals(170, sequence.getTerm(4));
    }

    @Test
    public void doubleDigitTerm() {
        fibonacciSequence sequence = new fibonacciSequence(5, 6);

        assertEquals(309, sequence.getTerm(10));
    }

    @Test
    public void getTerm1or2(){
        fibonacciSequence sequence = new fibonacciSequence(5, 6);

        assertEquals(5, sequence.getTerm(1));
    }
}
