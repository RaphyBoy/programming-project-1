import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
 
public class TESTsubtractonacciSequence{  
    
    @Test  
    public void normalInput(){
        subtractonacciSequence sequence = new subtractonacciSequence(1, 2);

        assertEquals(-1,sequence.getTerm(3));
    }

    @Test  
    public void negativeInput(){
        subtractonacciSequence sequence = new subtractonacciSequence(-1, -2);

        assertEquals(1,sequence.getTerm(3));
    }

    @Test  
    public void doubleDigitInputs(){
        subtractonacciSequence sequence = new subtractonacciSequence(10, 20);

        assertEquals(-10,sequence.getTerm(3));
    } 

    @Test  
    public void negAndPosInput(){
        subtractonacciSequence sequence = new subtractonacciSequence(1, -2);

        assertEquals(3,sequence.getTerm(3));
    }

    @Test  
    public void doubleDigitTerm(){
        subtractonacciSequence sequence = new subtractonacciSequence(-1, -2);
        assertEquals(-47,sequence.getTerm(10));
    }

    @Test
    public void getTerm1or2(){
        subtractonacciSequence sequence = new subtractonacciSequence(5, 6);

        assertEquals(5, sequence.getTerm(1));
    }

}

